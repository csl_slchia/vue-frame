// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// 入口文件 
import Vue from 'vue'
import App from './App'
import router from './router'
// import css
import 'vue-easytable/libs/themes-base/index.css'
import './assets/bootstrap-3.3.7/css/bootstrap.min.css'

import $ from 'jquery'
import './assets/bootstrap-3.3.7/js/bootstrap'
// import table and pagination comp
import {VTable,VPagination} from 'vue-easytable'
import uploader from 'vue-simple-uploader'

Vue.config.productionTip = false
// vue-table设置
Vue.component(VTable.name, VTable)
Vue.component(VPagination.name, VPagination)
Vue.use(uploader)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  render(createElement) {
    return createElement(App)
  }
})
